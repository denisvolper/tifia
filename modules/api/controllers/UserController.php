<?php

namespace app\modules\api\controllers;

use yii\helpers\VarDumper;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\db\Query;
use Yii;

class UserController extends Controller {

  public function actionGetReferalsTree($client_id) {

      $request = Yii::$app->request;

      if (!$request->isGet) {
        return Yii::$app->response->statusCode = 400;
      }

      $get = $request->get();
      $client_id = $get['client_id'];

      // Get client itself
      $client = (new Query())
        ->select('client_uid AS id, email, fullname AS text, partner_id AS parent')
        ->from('users')
        ->where(['client_uid' => $client_id])
        ->one();

      if (!$client) {
        return Yii::$app->response->statusCode = 400;
      }  
    
      // Get straight referals count
      $straight_referals = (new Query())
        ->select('id')
        ->from('users')
        ->where(['partner_id' => $client_id])
        ->count(); 
      
      // Get referals tree by partner_id
      // PS: Don't know, how to write it with query builder
      $users = Yii::$app->db->createCommand("WITH RECURSIVE cte (client_uid, email, fullname, partner_id, level) AS (
        SELECT    client_uid,
                  email,
                  fullname,				 
                  partner_id,
                  1
        FROM      users
        WHERE     partner_id = $client_id
        UNION ALL
        SELECT    u.client_uid,
                  u.email,
                  u.fullname,
                  u.partner_id,
                  level + 1
        FROM      users u
        INNER JOIN cte
                  ON u.partner_id = cte.client_uid)
      SELECT client_uid AS 'id', email, fullname AS 'text', partner_id AS 'parent', level from cte;")
      ->queryAll();

    array_push($users, $client);

    // Get max deep (grid levels)
    $levels = ArrayHelper::getColumn($users, 'level');
    $grid_levels = max($levels);

    // Get all referals accounts  
    $ids = ArrayHelper::getColumn($users, 'id'); 
    $accounts = (new Query())
      ->select('login')
      ->from('accounts')
      ->where(['in', 'client_uid', $ids])
      ->all();

    // Get required values  
    $summary_query = (new Query())
    ->select("SUM(profit) AS 'profit', SUM(volume * coeff_h * coeff_cr) AS 'volume'")
    ->from('trades')
    ->where(['in', 'login', $accounts]);
    
    if (isset($get['open_time']) && isset ($get['end_time'])) {
      $open_time = $get['open_time'];
      $close_time = $get['end_time'];
      
      // Format example: Sat Aug 11 2018 04:34:00 GMT+0300 (Москва, стандартное время)
      $summary_query->andWhere("open_time >= STR_TO_DATE('$open_time', '%a %b %d %Y %k:%i:%s')");
      $summary_query->andWhere("close_time <= STR_TO_DATE('$close_time', '%a %b %d %Y %k:%i:%s')");
    }
    $summary = $summary_query->one();
                    
    Yii::$app->response->statusCode = 200;
    return json_encode([
      'users' => $users,
      'values' => [
        'profit' => $summary['profit'],
        'volume' => $summary['volume'],
        'straight_referals' => $straight_referals,
        'all_referals' => count($users) - 1, // Minus client itself
        'grid_levels' => $grid_levels
      ]
    ]);
  }
}