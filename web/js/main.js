document.addEventListener("DOMContentLoaded", function () {
  const form = document.getElementById("mainForm");
  form.addEventListener("submit", async function (e) {
    e.preventDefault();
    const submitButton = form.querySelector("button[type=submit]");
    const dataContainer = document.querySelector("[data-container]");
    const loaderLabel = dataContainer.querySelector("[data-loader]");
    const valueContainer = dataContainer.querySelector("[data-values]");
    const treeContainer = dataContainer.querySelector("[data-tree]");

    // Reset all containers
    valueContainer.innerHTML = "";
    loaderLabel.classList.add("active");
    submitButton.setAttribute("disabled", true);

    const url = this.getAttribute("action");
    const clientId = this.elements.clientId.value;
    const openTime = this.elements.openTime.value;
    const endTime = this.elements.endTime.value;

    const params = {
      client_id: clientId,
    };
    if (openTime && endTime) {
      params.open_time = new Date(openTime);
      params.end_time = new Date(endTime);
    }

    const response = await fetch(`${url}?` + new URLSearchParams(params));

    submitButton.removeAttribute("disabled", true);
    loaderLabel.classList.remove("active");
    if (!response.ok) {
      alert("Ошибка HTTP: " + response.status);
    }

    let json = await response.json();
    const client = json.users.pop();

    const newData = [
      { id: client.id, parent: "#", text: client.text },
      ...json.users,
    ];

    // Update tree if already init
    if ($(treeContainer).jstree(true).settings) {
      $(treeContainer).jstree(true).settings.core.data = newData;
      $(treeContainer).jstree(true).refresh();
    } else {
      // Render tree in first
      $(treeContainer).jstree({
        core: {
          data: [
            { id: client.id, parent: "#", text: client.text },
            ...json.users,
          ],
        },
      });
    }

    // Render values
    for (key in json.values) {
      const li = document.createElement("li");
      li.innerHTML = `<strong>${key}:</strong> ${
        json.values[key] ? json.values[key] : 0
      }`;
      valueContainer.appendChild(li);
    }
  });
});
