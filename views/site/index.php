<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">
    <div class="body-content">

        <div class="row">
            <div class="col-lg-6">
                <form id="mainForm" type="GET" action="/api/user/get-referals-tree">
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label for="clientUid">id клиента (client_uid)</label>
                            <input type="number" class="form-control" id="clientUid" placeholder="client_uid" name="clientId" value="82824897" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="startDate">Начало периода</label>
                            <input type="datetime-local" class="form-control" name="openTime" placeholder="openTime">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="startDate">Конец периода</label>
                            <input type="datetime-local" class="form-control" name="endTime" placeholder="endTime">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <button type="submit" class="btn btn-primary text-right">
                            Посчитать реферальную сетку
                        </button>
                    </div>
                </form>
                <div class="col-md-12" data-container>
                    <div class="load-stub" data-loader>
                        Выполняется запрос. Пожалуйста подождите..
                    </div>
                    <div data-tree></div>
                    <ul data-values></ul>
                </div>
            </div>
        </div>

    </div>
</div>
